module clk_divider(clk,reset,i2c_clk);
  
  input reset,clk;
  output reg i2c_clk;
  
  reg[9:0]count = 0;
  
  parameter delay 1000;
  
  initial
    i2c_clk=0;
  
  always@(posedge clk)begin
    if(reset)begin
      i2c_clk<=0;
      count<=0;
    end else begin
      if(count==((delay/2)-1))begin //100Mhz clk to 1khz clk
        i2c_clk=~i2c_clk;
        count=0;
      end else begin
        count=count+1;
      end
  end